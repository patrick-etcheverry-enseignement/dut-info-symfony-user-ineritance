# Authentification avec des entités Utilisateur différentes  

## Contexte
Cet exemple de code montre comment gérer l'authentification lorsque on doit élaborer une application Symfony devant gérer différentes entités Utilisateur. 

Dans l'exemple présenté on dispose d'une entité **Medecin** et d'une entité **Patient** héritant de la classe **User** de Symfony et on souhaite permettre à des utilisateurs **Medecin** et des utilisateur **Patient** de s'authentifier. 
 
L'entité **User** a été créée avec la commande `make:user` de Symfony. Les entités **Patient** et **Medecin** ont été créées comme des entités classiques avec la commande `make:entity` puis modifiées à la main pour ajouter l'héritage avec l'entité **User**. Par exemple pour l'entite **Medecin** :
`class Medecin extends User`


## Problème
Le mécanisme d'authentification de Symfony repose sur l'entité **User** créée avec la commande `make:user`. Si on veut baser l'authentification sur d'autres entités (**Medecin** et **Patient** dans cet exemple), il faut paramétrer un peu l'héritage entre ces classes et la classe **User**


## Solution
La solution mise en place suit les étapes suivantes :
 
* On rend la classe **User** abstraite :  
    abstract class User implements UserInterface

* On paramètre l'héritage de sorte à ce que les attributs impliqués dans l'authentification (email et mot de passe) soient des champs de la table **User**. De ce fait, **Medecin** et **Patient** bénéficient du mécanisme d'authentification de **User**:
```
/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"medecin" = "Medecin", "patient" = "Patient"})
 */
abstract class User implements UserInterface
```

* Le formulaire d'authentification est ensuite créé de manière classique via l'assistant de Symfony (commande `make:auth`) 

